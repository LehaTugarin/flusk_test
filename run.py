from flask import Flask, render_template as render, request, redirect
from flask import jsonify
from tinydb import TinyDB, Query

app = Flask(__name__)

db = TinyDB('db.json')
db_sets=TinyDB('db_sets.json')

context = {'login': '', 'role':'guest', 'alert':''}
user_data = {'name':'','address':'','birthday':'','email':'','site':''}
shedule_mass= {'data_massiv':''}

# code

@app.route("/")
def index():
    return render('template.html', banner_small_text="Wellcome to Studio!",
        banner_big_text="Only high quality content", title="My Site",copyright="CopyRight 2018 Digital All Rights Reserved",**context)


#---------------------------auth----------------------
@app.route("/auth/")
def auth():
    return render('auth.html',  title="Auth",copyright="CopyRight 2018 Digital All Rights Reserved")

@app.route('/auth', methods=['POST'])
def do_auth():
    User = Query()
    users = db.search(User.login == request.form['login'])
    if len(users) > 0:
        user = users[0]
        if user['password'] == request.form['password']:
            context['login'] = user['login']
            context['role'] = 'admin'
            return redirect('/')
        else:
            context['alert']='bad password'
    else:
        context['alert']='user not foud...'
    return render('/', **context)

@app.route("/registration/")
def registration():
    return render('registration.html',  title="Registration",copyright="CopyRight 2018 Digital All Rights Reserved")

@app.route('/registration', methods=['POST'])
def do_registration():
    User = Query()
    users = db.search(User.login == request.form['login'])
    if len(users) > 0:
        context['alert'] = 'bad email('
        return render('registration.html', **context)
    else:
        db.insert({
            'login':request.form['login'],
            'email':request.form['email'],
            'password':request.form['password']
        })
        context['login'] = request.form['login']
        context['role'] = 'admin'

    return redirect('/registration_success')

@app.route("/registration_success")
def reg_success():
    return render('registration_success.html',  title="Gallyery",copyright="CopyRight 2018 Digital All Rights Reserved")

@app.route('/signout')
def signout():
    context['login'] = ''
    context['role'] = 'guest'
    return redirect('/')

#--------------------------------------------------

@app.route("/profile/")
def profile():
    User = Query()
    users = db.search(User.login == context['login'])
    if len(users) > 0:
        user = users[0]
        
        print(user)
        
        #hasattr not working(((
        if('name' in user):
            print('ok!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            user_data['name']= user['name']
        else:
            user_data['name']="no data..."

        
        if('address' in user):
            user_data['address']= user['address']
        else:
            user_data['address']="no data..."
        
        
        if('birthday'in user):
            user_data['birthday']= user['birthday']
        else:
            user_data['birthday']="no data..."
        

        
        if('site' in user):   
            user_data['site']= user['site']
        else:
            user_data['site']="no data..."
        
        user_data['email']= user['email']


        
    else:
        context['alert']='user not foud...'
    #return render('/', **context)
    return render('profile.html',  title="Profile",copyright="CopyRight 2018 Digital All Rights Reserved",**user_data)


@app.route('/edit_profile',methods=['POST'])
def ed_profile():
    User = Query()
    users = db.search(User.login == context['login'])

    print(request.form)
    
    db.update({
        'birthday':request.form['birthday'],
        'name':request.form['name'],
        'address':request.form['address'],
        'email':request.form['email'],
        'site':request.form['site']
    }, User.login == context['login'])
    return redirect('/profile')

@app.route("/edit_profile/")
def edit_profile():
    return render('edit-profile.html',  title="Edit fotoset", **user_data)

#--------------------------------------------------
@app.route('/add_set',methods=['POST'])
def add_pset():
    if(request.form['name']):
        db_sets.insert({
            'name':request.form['name'],
            'date':request.form['date'],
            'time':request.form['time']
        })


    return redirect('/shedule')

@app.route("/shedule/")
def shedule():
    shedule_mass['data_massiv']=db_sets.all()
    print(shedule_mass)
    return render('shedule.html',  title="Shedule",copyright="CopyRight 2018 Digital All Rights Reserved",shedule_mass=shedule_mass)


#--------------------------------------------------








@app.route("/gallery/")
def gallery():
    return render('gallery.html',  title="Gallyery",copyright="CopyRight 2018 Digital All Rights Reserved")

@app.route("/about/")
def about():
    return render('about.html',  title="About",copyright="CopyRight 2018 Digital All Rights Reserved")


@app.route("/add_fotoset/")
def add_fotoset():
    return render('add-fotoset.html',  title="Add fotoset")

@app.route("/edit_fotoset/")
def edit_fotoset():
    return render('edit-fotoset.html',  title="Edit fotoset")



if __name__ == '__main__':
    app.run(debug=True)
